/*
 * DoubleLinkedList.java
 * Copyright (C) 2017 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 * TODO
 */

package list;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoubleLinkedList<T> implements List<T>, Iterable<T>
{
    Node<T> head;
    Node<T> tail;
    public DoubleLinkedList() {
        head = null;
        tail = null;
    }

    public DoubleLinkedList(ArrayList<T> arr)
    {
        for (T element : arr) {
            this.insertAtTail(element);
        }
    }

    public int findKeyPosition(T key) {
        int pos = 0;
        for (T t : this)
        {
            if (t.equals(key))
            {
                return pos;
            }
            pos++;
        }
        return -1;
    }

    public void insertAtHead(T key)
    {
        if (head == null)
        {
            head = new Node<T>(key);
            tail = head;
        }
        else
        {
            Node<T> oldHead = head;
            head = new Node<T>(key);
            head.setNext(oldHead);
            oldHead.setPrevious(head);
        }
    }

    public void insertAtTail(T key)
    {
        if (head == null)
        {
            insertAtHead(key);
        }
        else
        {
            Node<T> oldTail = tail;
            tail = new Node<T>(key);
            oldTail.setNext(tail);
            tail.setPrevious(oldTail);
        }
    }

    public void insertAt(T key, int position)
    {
        if (position == 0)
            insertAtHead(key);

        // If it's after the last element
        else if (position == this.size())
            insertAtTail(key);
        else if (position > this.size())
            throw new IndexOutOfBoundsException();
        else
        {
            int i = 0;
            Node<T> target = null;
            for (DoubleLinkedListIterator current = new DoubleLinkedListIterator(); current.hasNext();)
            {
                if (i == position - 1)
                {
                    target = current.getNode();
                    // System.out.println("Prev: "+target.getPrevious());
                    // System.out.println("This: "+target);
                    // System.out.println("Next: "+target.getNext());
                    // System.out.println("Curr Pos: "+ i);
                    break;
                }
                i++;
                current.next();
            }
            Node<T> newNode = new Node<T>(key);
            newNode.setPrevious(target);
            newNode.setNext(target.getNext());
            target.setNext(newNode);
            target.getNext().setPrevious(newNode);
        }
    }

    public T peekHead()
    {
        if (head == null)
            throw new IndexOutOfBoundsException();
        else
            return head.getData();
    }

    public T peekTail()
    {
        if (tail == null)
            throw new IndexOutOfBoundsException();
        else
            return tail.getData();
    }

    public T peekAt(int position)
    {
        if (position == 0)
            return peekHead();
        else if (position == this.size() - 1)
            return peekTail();
        else
        {
            int i = 0;
            for (T current : this)
            {
                if (i == position)
                {
                    return current;
                }
                i++;
            }
            throw new IndexOutOfBoundsException();
        }
    }

    public T getHead()
    {
        if (head == null)
            throw new IndexOutOfBoundsException();
        else
        {
            Node<T> oldHead = head;
            head = oldHead.getNext();
            return oldHead.getData();
        }
    }
    public T getTail()
    {
        if (tail == null)
            throw new IndexOutOfBoundsException();
        else
        {
            Node<T> oldTail = tail;
            tail = oldTail.getPrevious();
            tail.setNext(null);
            return oldTail.getData();
        }

    }
    public T getAt(int position)
    {
        if (position == 0)
            return getHead();
        else if (position == this.size() - 1)
            return getTail();
        else if (position >= this.size())
            throw new IndexOutOfBoundsException();
        else
        {
            int i = 0;
            Node<T> target = null;
            for (DoubleLinkedListIterator current = new DoubleLinkedListIterator(); current.hasNext();)
            {
                if (i == position)
                {
                    target = current.getNode();
                    break;
                }
                i++;
                current.next();
            }
            target.getPrevious().setNext(target.getNext());
            target.getNext().setPrevious(target.getPrevious());
            return target.getData();
        }
    }

    // TODO: Should we just let insert?
    // Currently not.
    public void setHead(T key)
    {
        if (head == null)
            throw new IndexOutOfBoundsException();
        else
        {
            Node<T> oldHead = head;
            head = new Node<T>(key);
            head.setNext(oldHead.getNext());
            oldHead.getNext().setPrevious(head);
        }
    }

    public void setTail(T key)
    {
        if (tail == null)
            throw new IndexOutOfBoundsException();
        else
        {
            Node<T> oldTail = tail;
            tail = new Node<T>(key);
            tail.setPrevious(oldTail.getPrevious());
        }
    }

    public void setAt(T key, int position)
    {
        if (position == 0)
            setHead(key);
        else if (position == this.size() - 1)
            setTail(key);
        else if (position >= this.size())
            throw new IndexOutOfBoundsException();
        else
        {
            int i = 0;
            Node<T> target = null;
            for (DoubleLinkedListIterator current = new DoubleLinkedListIterator(); current.hasNext();)
            {
                if (i == position)
                {
                    target = current.getNode();
                    break;
                }
                i++;
                current.next();
            }
            Node<T> newNode = new Node<>(key);
            newNode.setPrevious(target.getPrevious());
            newNode.setNext(target.getNext());
            target.getPrevious().setNext(newNode);
            target.getNext().setPrevious(newNode);
        }
    }

    public int size()
    {
        if (head == null)
            return 0;
        int num = 0;
        for (T current : this)
        {
            num++;
            // System.out.println("This:" +current);
            // System.out.println(num);
        }
        return num;
    }

    @Override
    public String toString()
    {
        StringBuilder ans = new StringBuilder();
        if (head == null)
            return "Empty List";
        for (T current : this)
        {
            ans.append(current);
            ans.append(" -> ");
        }
        return ans.toString();
    }

    public ArrayList<T> toArrayList() {
        ArrayList<T> ans = new ArrayList<>(this.size());
        if (head == null)
            return null;
        for (T current : this)
        {
            ans.add(current);
        }
        return ans;
    }

    @Override
    public Iterator<T> iterator()
    {
        return new DoubleLinkedListIterator();
    }

    public class DoubleLinkedListIterator implements Iterator<T>
    {
        private Node<T> nextNode;

        public DoubleLinkedListIterator()
        {
            nextNode = head;
        }

        public boolean hasNext()
        {
            return nextNode != null;
        }

        public T next()
        {
            if (!hasNext())
                throw new NoSuchElementException();
            T ans = nextNode.getData();
            nextNode = nextNode.getNext();
            return ans;
        }

        public Node<T> getNode()
        {
            return nextNode;
        }

    }

}
