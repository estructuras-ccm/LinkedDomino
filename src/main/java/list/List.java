/*
 * List.java
 * Copyright (C) 2017 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 * TODO: Implement iterable
 */
package list;
public interface List<T>
{
    public int findKeyPosition(T key);

    public void insertAtHead(T key);
    public void insertAtTail(T key);
    public void insertAt(T key, int position);

    public T peekHead();
    public T peekTail();
    public T peekAt(int position);

    public T getHead();
    public T getTail();
    public T getAt(int position);

    public void setHead(T key);
    public void setTail(T key);
    public void setAt(T key, int position);

    public int size();

    @Override
    public String toString();
}
