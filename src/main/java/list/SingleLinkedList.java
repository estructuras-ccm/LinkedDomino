/*
 * singleLinkedList.java
 * Copyright (C) 2017 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 * TODO: Is .getNext().getNext() valid? Maybe use parent
 * TODO: Maybe make it double ended
 * Bug: Insert at 0 when empty makes it at head, inconsistent behavior.
 * Potential bug: Maybe returns null instead of IndexOutOfBoundsException when index is big, but list is empty
 *
 */

package list;
import java.util.ArrayList;
import java.util.Random;


public class SingleLinkedList<T> implements List<T>
{
    Node<T> head;
    Node<T> tail;
    int size;
    Node<T> iterator;
    public SingleLinkedList()
    {
        head = null;
        tail = null;
    }

    public SingleLinkedList(ArrayList<T> arr)
    {
        for (T element : arr ) {
            this.insertAtTail(element);
        }
    }

    public int findKeyPosition(T key)
    {
        Node<T> aNode = head;
        int position = 0;
        if (head.getData() == key) return 0;
        if (head == null) return -1;
        //We need to go until last element
        while(aNode != null)
        {
            if (aNode.getData().equals(key))
            {
                return position;
            }
            else
            {
                aNode = aNode.getNext();
                position++;
            }
        }
        return -1;
    }

    public void insertAtHead(T key)
    {
        Node<T> oldHead = head;
        head = new Node<T>(key);
        head.setNext(oldHead);
        if (oldHead == null) tail = head;
    }

    public void insertAtTail(T key)
    {
        if (head == null)
        {
            head = new Node<T>(key);
            tail = head;
        }

        else {
            tail.setNext(new Node<T>(key));
            tail = tail.getNext();
        }
    }

    public void insertAt(T key, int endPosition)
    {
        if(head != null && endPosition > this.size()) throw new IndexOutOfBoundsException();
        Node<T> aNode = head;
        int position = 0;

        if(endPosition == 0)
        {
            insertAtHead(key);
        }
        else if(endPosition == this.size())
        {
            insertAtTail(key);
        }
        else {
            while (position < endPosition-1)
            {
                aNode = aNode.getNext();
                position++;
            }
            Node<T> prev = aNode;
            Node<T> newNode = new Node<T>(key);
            newNode.setNext(aNode.getNext());
            aNode.setNext(newNode);
        }
    }

    public T peekHead()
    {
        if (head == null) return null;
        else return head.getData();
    }

    public T peekTail()
    {
        if (head == null) return null;
        return tail.getData();
    }

    public T peekAt(int position)
    {
        if (head == null) return null;
        else if (position >= this.size()) throw new IndexOutOfBoundsException();
        Node<T> aNode = head;
        int index = 0;
        while (aNode.getNext() != null && index < position)
        {
            aNode = aNode.getNext();
            index++;
        }
        return aNode.getData();
    }

    public T getHead()
    {
        if (head == null) return null; //Should we return null or exception?
        Node<T> oldHead = head;
        head = head.getNext();
        return oldHead.getData();
    }

    public T getTail()
    {
        if (head == null) return null; //Should we return null or exception?
        if (head.getNext() == null){
            Node<T> oldTail = head;
            head = null;
            return oldTail.getData();
        }
        Node<T> aNode = head;
        while (aNode.getNext().getNext() != null)
        {
            aNode = aNode.getNext();
        }
        Node<T> oldTail = aNode.getNext();
        aNode.setNext(null);
        tail = aNode;
        return oldTail.getData();
    }


    public T getAt(int n)
    {
        if (head == null) return null;
        if (n >= this.size()) throw new IndexOutOfBoundsException();
        if (n == 0) return this.getHead();
        Node<T> aNode = head;
        int index = 0;
        while (index < n-1)
        {
            aNode = aNode.getNext();
            index++;
        }
        Node<T> oldNode = aNode.getNext();
        aNode.setNext(oldNode.getNext());
        return oldNode.getData();
    }

    public void setHead(T key)
    {
        if(head == null)
        {
            this.insertAtHead(key);
        }
        else
        {
            Node<T> nextToHead = head.getNext();
            head = new Node<T>(key);
            head.setNext(nextToHead);
        }
    }

    public void setTail(T key)
    {
        if (head == null)
        {
            this.insertAtTail(key);
        }
        else
        {
            Node<T> aNode = head;
            while (aNode.getNext().getNext() != null)
            {
                aNode = aNode.getNext();
            }
            aNode.setNext(new Node<T>(key));
            tail = aNode.getNext();
        }
    }

    public void setAt(T key, int position)
    {
        if (position == 0)
        {
            setHead(key);
        }
        else if (position == this.size()-1)
        {
            setTail(key);
        }
        else if (position >= this.size())
        {
            throw new IndexOutOfBoundsException();
        }
        else
        {
            Node<T> aNode = head;
            int index = 0;
            while (index < position-1)
            {
                index++;
                aNode = aNode.getNext();
            }
            Node<T> newNode = new Node<T>(key);
            newNode.setNext(aNode.getNext().getNext());
            aNode.setNext(newNode);
        }
    }

    public T iterate()
    {
        if(head == null) throw new IndexOutOfBoundsException();;
        if(iterator == null) iterator = head;
        if(iterator.getNext() == null) return null;
        Node<T> oldIterator = iterator;
        iterator = iterator.getNext();
        return oldIterator.getData();
    }

    public T peekCurrent()
    {
        if (head == null) throw new IndexOutOfBoundsException();
        if (iterator == null) iterator = head;
        return iterator.getData();
    }

    public int size()
    {
        //TODO Optimize?
        int ans = 0;
        if (head != null)
        {
            Node<T> aNode = head;
            while(aNode != null)
            {
                aNode = aNode.getNext();
                ans++;
            }
        }
        return ans;
    }

    public void resetIterator()
    {
        iterator = head;
    }

    @Override
    public String toString()
    {
        StringBuilder tmp = new StringBuilder();
        Node<T> aNode = head;
        while(aNode != null)
        {
            tmp.append(aNode.getData());
            tmp.append(" -> ");
            aNode = aNode.getNext();

        }
        return tmp.toString();
    }

    public ArrayList<T> toArrayList(){
        ArrayList<T> ans = new ArrayList<>(this.size());
        if (head == null) return null;
        Node<T> current = head;
        while(current != null){
            ans.add(current.getData());
            current = current.getNext();
        }
        return ans;
    }

    //Fisher-Yates shuffle
    public void shuffle()
    {
        Random rand = new Random();
        ArrayList<T> ans = this.toArrayList();
        // System.out.println("Size: "+ans.size());
        for (int i = 0; i < ans.size(); i++) {
            int j = rand.nextInt(ans.size());
            T tmp = ans.get(i);
            // System.out.println("1: "+tmp);
            ans.set(i, ans.get(j));
            ans.set(j, tmp);
            // System.out.println("2: "+tmp);
            // System.out.println(i);
        }
        SingleLinkedList<T> newList = new SingleLinkedList<T>(ans);
        head = newList.head;
    }

    //TODO: Add to Interface
    public boolean isEmpty()
    {
        return (head == null);
    }
}
