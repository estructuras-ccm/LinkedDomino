/*
 * SortedLinkedList.java
 * Copyright (C) 2017 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 */

package list;
public class SortedLinkedList<T extends Comparable<T>> implements SortedList<T>
{
    Node<T> head;

    public SortedLinkedList() {
        head = null;
    }

    public int findKeyPosition(T key)
    {
        Node<T> aNode = head;
        int position = 0;
        if (head.getData() == key) return 0;
        if (head == null) return -1;
        //We need to go until last element
        while(aNode != null)
        {
            if (aNode.getData().equals(key))
            {
                return position;
            }
            else
            {
                aNode = aNode.getNext();
                position++;
            }
        }
        return -1;
    }

    private void insertHead(T key)
    {
        Node<T> oldHead = head;
        head = new Node<T>(key);
        head.setNext(oldHead);
    }

    private void insertTail(T key)
    {
        Node<T> current = head;
        while(current.getNext() != null)
        {
            current = current.getNext();
        }
        Node<T> newNode = new Node<T>(key);
        newNode.setNext(null);
        current.setNext(newNode);
    }

    public void insert(T key)
    {
        if (head == null) head = new Node<T>(key);
        else
        {
            Node<T> current = head;
            if (key.compareTo(current.getData())<1)
            {
                insertHead(key);
            }
            else
            {
                while(current.getNext() != null &&
                        key.compareTo(current.getNext().getData()) >0)
                {
                    current = current.getNext();
                }
                if(current.getNext() == null)
                {
                    insertTail(key);
                }
                else
                {
                    Node<T> newNode = new Node<T>(key);
                    newNode.setNext(current.getNext());
                    current.setNext(newNode);
                }
            }
        }
    }

    public T peekHead()
    {
        if (head == null) return null;
        return head.getData();
    }

    public T peekTail()
    {
        if (head == null) return null;
        else
        {
            Node<T> current = head;
            while(current.getNext() != null)
            {
                current = current.getNext();
            }
            return current.getData();
        }
    }

    public T peekAt(int position)
    {
        if (head == null || position >= this.size())
            throw new IndexOutOfBoundsException();
        else
        {
            int currPos = 0;
            Node<T> current = head;
            while(currPos < position)
            {
                currPos++;
                current = current.getNext();
            }
            return current.getData();
        }
    }

    public T getHead()
    {
        if (head == null) throw new IndexOutOfBoundsException();
        Node<T> oldHead = head;
        head = head.getNext();
        return oldHead.getData();
    }

    public T getTail(){
        if (head == null) throw new IndexOutOfBoundsException();
        if (head.getNext() == null)
        {
            Node<T> oldHead = head;
            head = null;
            return oldHead.getData();
        }
        Node<T> aNode = head;
        while (aNode.getNext().getNext() != null)
        {
            aNode = aNode.getNext();
        }
        Node<T> oldTail = aNode.getNext();
        aNode.setNext(null);
        return oldTail.getData();
    }

    public T getAt(int position){
        if (head == null) return null;
        if (position >= this.size()) throw new IndexOutOfBoundsException();
        if (position == 0) return this.getHead();
        Node<T> aNode = head;
        int index = 0;
        while (index < position-1)
        {
            aNode = aNode.getNext();
            index++;
        }
        Node<T> oldNode = aNode.getNext();
        aNode.setNext(oldNode.getNext());
        return oldNode.getData();
    }


    public int size()
    {
        //TODO Optimize?
        int ans = 0;
        if (head != null)
        {

            Node<T> aNode = head;
            while(aNode != null)
            {
                aNode = aNode.getNext();
                ans++;
            }
        }
        return ans;
    }

    @Override
    public String toString()
    {
        StringBuffer tmp = new StringBuffer();
        Node<T> currentNode = head;
        if (head==null ) return "";
        while (currentNode != null)
        {
            tmp.append(currentNode);
            tmp.append(" -> ");
            currentNode = currentNode.getNext();
        }
        return tmp.toString();
    }


}

