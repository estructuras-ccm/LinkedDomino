/*
 * SortedList.java
 * Copyright (C) 2017 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 */
package list;
public interface SortedList<T extends Comparable<T>>
{
    public int findKeyPosition(T key);

    public void insert(T key);

    public T peekHead();
    public T peekTail();
    public T peekAt(int position);

    public T getHead();
    public T getTail();
    public T getAt(int position);

    public int size();

    @Override
    public String toString();


}

