/*
 * Node.java
 * Copyright (C) 2017 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 */

package list;
import java.util.Iterator;
public class Node<T>
{
    private final T data;
    private Node<T> previous;
    private Node<T> next;

    public Node(T object)
    {
        data = object;
    }

    //@Deprecated
    public Node<T> getPrevious()
    {
        return previous;
    }

    //@Deprecated
    public Node<T> getNext()
    {
        return next;
    }

    public void setNext(Node<T> node)
    {
        next = node;
    }

    public void setPrevious(Node<T> node)
    {
        previous = node;
    }

    public T getData()
    {
        return data;
    }

    @Override
    public String toString()
    {
        return data.toString();
    }



}
