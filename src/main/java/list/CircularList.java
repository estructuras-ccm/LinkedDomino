/*
 * CircularList.java
 * Copyright (C) 2017 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 */

package list;
public class CircularList<T> implements List<T>
{
    Node<T> head;
    Node<T> iterator;
    public CircularList() {
        head = null;
        iterator = null;
    }

    public int findKeyPosition(T key)
    {
        Node<T> aNode = head.getNext();
        int position = 1;
        if (head.getData() == key) return 0;
        if (head == null) return -1;
        //We need to go until last element
        while(aNode != head)
        {
            if (aNode.getData().equals(key))
            {
                return position;
            }
            else
            {
                aNode = aNode.getNext();
                position++;
            }
        }
        return -1;
    }

    public void insertAtHead(T key)
    {
        if (head == null)
        {
            head = new Node<T>(key);
            head.setNext(head);
        }
        else {
            Node<T> oldHead = head;
            head = new Node<T>(key);
            head.setNext(oldHead);
            // head.getNext().setNext(head);

            // Set Tail to head
            Node<T> current = oldHead.getNext();
            // Go to last element
            for (current = oldHead.getNext(); current.getNext() != oldHead;current=current.getNext() ){}

            current.setNext(head);
        }
    }

    public void insertAtTail(T key)
    {
        if (head == null)
        {
            this.insertAtHead(key);
        }
        else
        {

            Node<T> current = head.getNext();
            Node<T> newNode = new Node<T>(key);
            while(current.getNext()!= head)
            {
                current= current.getNext();
            }
            current.setNext(newNode);
            newNode.setNext(head);
        }
    }


    public void insertAt(T key, int position)
    {
        if(position > this.size()) throw new IndexOutOfBoundsException();
        else if(position == 0) this.insertAtHead(key);
        /* else if(position == this.size()-1) this.insertAtTail(key); */
        else
        {
            int currPos = 0;
            Node<T> newNode = new Node<T>(key);
            if (position == 0) insertAtHead(key);
            else
            {
                Node<T> current = head;
                while (currPos < position-1)
                {
                    currPos++;
                    current = current.getNext();
                }
                newNode.setNext(current.getNext());
                current.setNext(newNode);
            }

        }
    }


    public T peekHead()
    {
        if (head == null) return null;
        return head.getData();
    }

    public T peekTail()
    {
        if (head == null) return null;
        Node<T> current = head;
        while (current.getNext() != head)
        {
            current = current.getNext();
        }
        return current.getData();
    }
    public T peekAt(int position)
    {
        if (head == null) return null;
        if (position >= this.size()) throw new IndexOutOfBoundsException();
        int curPos = 0;
        Node<T> current = head;
        while (curPos < position)
        {
            current = current.getNext();
            curPos++;
        }
        return current.getData();
    }

    public T getHead()
    {
        if (head == null) return null;
        Node<T> oldHead = head;
        head = head.getNext();
        Node<T> current = head.getNext();
        while (current.getNext() != oldHead)
        {
            current = current.getNext();
        }
        current.setNext(head);
        return oldHead.getData();
    }

    public T getTail()
    {
        if (head == null) return null;
        Node<T> current = head;
        while (current.getNext().getNext() != head)
        {
            current = current.getNext();
        }
        Node<T> oldTail = current.getNext();
        current.setNext(head);
        return oldTail.getData();

    }
    public T getAt(int position)
    {
        if (head == null) return null;
        if (position >= this.size()) throw new IndexOutOfBoundsException();
        int currPos = 0;
        Node<T> current = head;
        while(currPos < position-1)
        {
            currPos++;
            current = current.getNext();
        }
        Node<T> oldNode = current.getNext();
        current.setNext(oldNode.getNext());
        return oldNode.getData();
    }

    public void setHead(T key)
    {
        if (head == null)
        {
            this.insertAtHead(key);
        }
        else
        {
            Node<T> oldHead = head;
            head = new Node<T>(key);
            head.setNext(oldHead.getNext());
            Node<T> current = oldHead.getNext();
            while (current.getNext() != oldHead)
            {
                current = current.getNext();
            }
            current.setNext(head);
        }
    }


    public void setTail(T key)
    {
        if (head == null)
        {
            this.insertAtTail(key);
        }
        else
        {
            Node<T> current = head;
            while(current.getNext().getNext() != head)
            {
                current = current.getNext();
            }
            Node<T> newNode = new Node<T>(key);
            current.setNext(newNode);
            newNode.setNext(head);
        }
    }

    public void setAt(T key, int position)
    {
        if (position == 0) setHead(key);
        if (position >= this.size()) throw new IndexOutOfBoundsException();
        else
        {
        Node<T> current = head;
        int currPos = 0;
        while(currPos < position-1)
        {
            currPos++;
            current = current.getNext();
        }
        Node<T> newNode = new Node<T>(key);
        newNode.setNext(current.getNext().getNext());
        current.setNext(newNode);
        }
    }

    public T iterate()
    {
        if(head == null) throw new IndexOutOfBoundsException();;
        if(iterator == null) iterator = head;
        if(iterator.getNext() == null) return null;
        Node<T> oldIterator = iterator;
        iterator = iterator.getNext();
        return oldIterator.getData();

    }

    public int size()
    {
        int ans = 0;
        Node<T> current = head;
        if(head == null) return 0;
        else
        {
            do
            {
                ans++;
                current = current.getNext();
            } while (current != head);
        }
        return ans;
    }

    public T peekCurrent()
    {
        if (head == null) throw new IndexOutOfBoundsException();
        if (iterator == null) iterator = head;
        return iterator.getData();
    }

    //TODO: Untested
    //Moves to first occurence of T
    public void moveCurrent(T target)
    {
        iterator = head;
        if (iterator == null) throw new IndexOutOfBoundsException();
        int num = findKeyPosition(target);
        for (int i = 0; i < num; i++) {
            iterator = iterator.getNext();
        }
    }

    //TODO: Untested
    public void moveCurrent(int pos)
    {
        iterator = head;
        if (iterator == null) throw new IndexOutOfBoundsException();
        for (int i = 0; i < pos; i++) {
            iterator = iterator.getNext();
        }
    }

    public void resetIterator()
    {
        if (head == null) throw new IndexOutOfBoundsException();
        iterator = head;
    }

    @Override
    public String toString()
    {
        Node<T> current = head;
        StringBuilder tmp = new StringBuilder();
        do
        {
            tmp.append(current);
            tmp.append(" -> ");
            current = current.getNext();
        } while (current != head);
        return tmp.toString();
    }


}
