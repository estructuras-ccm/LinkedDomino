/*
 * Player.java
 * Copyright (C) 2017 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 */
import list.*;
import java.util.Scanner;
import list.SingleLinkedList;

public class Player
{
    SingleLinkedList<Pieza> myPieces;
    String name;
    Scanner scanner = new Scanner(System.in);

    public Player(String aName)
    {
        myPieces = new SingleLinkedList<>();
        name = aName;
    }

    public void addPieza(Pieza p)
    {
        myPieces.insertAtHead(p);
    }

    public Pieza putPieza()
    {
        System.out.println("What piece # to place");
        Scanner scanner = new Scanner(System.in);
        int place = scanner.nextInt();
        return myPieces.getAt(place - 1);
    }

    public DominoGame.Directions chooseDirection()
    {
        System.out.println("Place left (-1) or right (1)?");
        Scanner scanner = new Scanner(System.in);
        int direction = scanner.nextInt();
        if (direction < 0)
            return DominoGame.Directions.LEFT;
        else
            return DominoGame.Directions.RIGHT;
    }

    // Returns null if player has no mula
    // Implement iterator in List
    public boolean hasMula()
    {
        if (myPieces.isEmpty()) return false;
        int size = myPieces.size();
        for (int i = 0; i < size; i++) {
            if (myPieces.peekAt(i).esMula())
            {
                return true;
            }
        }
        return false;
    }

    public boolean hasMula(Pieza.Numeros num)
    {
        if (myPieces.isEmpty()) return false;
        int size = myPieces.size();
        for (int i = 0; i < size; i++) {
            if (myPieces.peekAt(i).getNumeroA()==num && myPieces.peekAt(i).esMula())
                {
                    return true;
                }
        }
        return false;
    }

    //TODO: Test
    public int highestMula()
    {
        if (myPieces.isEmpty()) return 0;
        int size = myPieces.size();
        int max = 0;
        for (int i = 0; i < size; i++) {
            if (myPieces.peekAt(i).toIntA()>max && myPieces.peekAt(i).esMula())
                {
                    max = myPieces.peekAt(i).toIntA();
                }
        }
        return max;
    }

    public void throwAwayPieces()
    {
        myPieces = new SingleLinkedList<Pieza>();
    }


    public DominoGame.Choices makeChoice()
    {
        System.out.println("Your tiles are:");
        System.out.println(myPieces);
        System.out.println("Your points are: ");
        System.out.println(getPoints());
        System.out.println("Input 1 for placing a piece");
        System.out.println("Input 2 to skip your turn");
        System.out.println("Input 3 to take a piece from pool");
        int direction = scanner.nextInt();
        switch (direction)
        {
            case 1 :
                return DominoGame.Choices.Play;
            case 2 :
                return DominoGame.Choices.Skip;
            case 3 :
                return DominoGame.Choices.TakePiece;
            default :
                return null;
        }
    }

    public Pieza choosePieza()
    {
        System.out.println("Enter piece number to place");
        int piece = scanner.nextInt()-1;
        if (piece >= myPieces.size() || piece <0)
            {
                throw new  IndexOutOfBoundsException();
            }
        else
            {
                return myPieces.getAt(piece);
            }
    }
    public boolean hasPiezas()
    {
        return !myPieces.isEmpty();
    }

    public int getPoints()
    {
        int ans = 0;
        if (myPieces.isEmpty()) return 0;
        myPieces.resetIterator();
        Pieza current;
        while ((current = myPieces.iterate()) != null)
            {
                ans += current.toIntA();
                ans += current.toIntB();
            }
        return ans;
    }

    public String getName()
    {
        return name;
    }

    public String toString()
    {
        return name;
    }

}
