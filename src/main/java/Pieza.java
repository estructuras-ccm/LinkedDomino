public class Pieza implements Comparable<Pieza> {
    private Numeros numA, numB;

    public enum Numeros
    {
        CERO,
        UNO,
        DOS,
        TRES,
        CUATRO,
        CINCO,
        SEIS
    }

    Pieza(Numeros _num1, Numeros _num2)
    {
        // if (_num1.compareTo(_num2) > 0)
        // {
            numA = _num1;
            numB = _num2;
        // }
        // else
        // {
            // numA = _num2;
            // numB = _num1;
        // }
    }

    public Numeros getNumeroA()
    {
        return numA;
    }

    public Numeros getNumeroB()
    {
        return numB;
    }

    public boolean esMula()
    {
        return (numA.equals(numB));
    }

    @Override
    public String toString() {
        return "[" + numA + ":" + numB + "]";
    }

    public void voltear()
    {
        Numeros tmp = numA;
        numA = numB;
        numB = tmp;
    }

    public int toIntA()
    {
        switch (numA)
            {
            case CERO:
                return 0;
            case UNO:
                return 1;
            case DOS:
                return 2;
            case TRES:
                return 3;
            case CUATRO:
                return 4;
            case CINCO:
                return 5;
            case SEIS:
                return 6;
            default:
                return -1;
            }
    }

    public int toIntB()
    {
        switch (numA)
            {
            case CERO:
                return 0;
            case UNO:
                return 1;
            case DOS:
                return 2;
            case TRES:
                return 3;
            case CUATRO:
                return 4;
            case CINCO:
                return 5;
            case SEIS:
                return 6;
            default:
                return -1;
            }
    }

    @Override
    public int compareTo(Pieza otraPieza)
    {
        if (this.numA == otraPieza.getNumeroA())
        {
            return numB.compareTo(otraPieza.getNumeroB());
        }
        else
        {
            return numA.compareTo(otraPieza.getNumeroA());
        }
    }

}
