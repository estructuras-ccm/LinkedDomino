/*
 * DominoGame.java
 * Copyright (C) 2017 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 * Right Game will be Tail
 * Left Game will be Head
 * This is the Control Class and contains players, the game itself, and the soup.
 */
import list.CircularList;
import list.SingleLinkedList;
import java.util.Scanner;

public class DominoGame
{
    SingleLinkedList<Pieza> piezasSopa;
    CircularList<Player> players;
    SingleLinkedList<Pieza> piezasJuego;
    Scanner scanner = new Scanner(System.in);
    // Ugly global variable
    int surrendered;
    boolean contGame;

    public DominoGame() {
        piezasSopa = new SingleLinkedList<>();
        piezasJuego = new SingleLinkedList<>();
        players = new CircularList<>();
        contGame = true;
    }

    public enum Directions
    {
        LEFT,
        RIGHT
    }

    public enum Choices
    {
        Play,
        Skip,
        TakePiece
    }

    private void setUpPiezas()
    {
        for (Pieza.Numeros num1 : Pieza.Numeros.values()) {
            for (Pieza.Numeros num2 : Pieza.Numeros.values()) {
                if (num2.compareTo(num1) >= 0)
                {
                    piezasSopa.insertAtTail(new Pieza(num1, num2));
                }
            }
        }
    }

    public void choooseHighestPlayer()
    {
        int maxTile = -1;
        int indexTile = -1;
        int minPoints = players.peekHead().getPoints();
        int indexPoints = 0;

        for (int i = 0; i < players.size(); i++) {
            Player current = players.iterate();
            System.out.println(current.myPieces);
            if (current.highestMula() > maxTile)
            {
                maxTile = current.highestMula();
                indexTile = i;
            }
            if (current.getPoints() < minPoints)
            {
                minPoints = current.getPoints();
                indexPoints = i;
            }
        }
        System.out.println("Highest Mula: " +maxTile);
        System.out.println("Lowest Points: "+minPoints);
        players.moveCurrent(indexTile > -1 ? indexTile : indexPoints);
    }

    public void gameCycle()
    {
        Player playerRound = players.peekCurrent();
        Player currentPlayer = players.iterate();
        while (contGame)
        {
            if (currentPlayer == playerRound)
                surrendered = 0;
            clearScreen();
            System.out.println("The map is:");
            System.out.println(piezasJuego);
            playerTurn(currentPlayer);
            if (!currentPlayer.hasPiezas())
            {
                wonGame(currentPlayer);
            }
            if (surrendered >= players.size())
            {
                getWinnerByPoints();
            }
            currentPlayer = players.iterate();
        }
    }

    public void getWinnerByPoints()
    {
        int minPoints = 100;
        int indexPoints = 0;
        int currentIndex = 0;
        players.resetIterator();
        Player current = players.iterate();
        do
        {
            // System.out.println("Player");
            // System.out.println(current);
            // System.out.println(current.myPieces);
            // System.out.println(current.getPoints());
            if (current.getPoints() < minPoints)
            {
                minPoints = current.getPoints();
                indexPoints = currentIndex;
            }
            else if (current.getPoints() == minPoints && minPoints != 100)
            {
                // System.out.println("The game is a match");
                endGame();
                break;
            }
            currentIndex++;
        } while ((current = players.iterate()) != players.peekHead());
        // System.out.println("Points: " +minPoints);
        // System.out.println("At: "+indexPoints);
        // System.out.println("Is: "+players.peekAt(indexPoints));

        wonGame(players.peekAt(indexPoints));
    }
    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public void wonGame(Player aPlayer)
    {
        System.out.println("Player: " + aPlayer.getName() + " has won");
        endGame();
    }

    public void endGame()
    {
        System.out.println("Do you wish to play another round? 0/1");
        int num = scanner.nextInt();
        if (num == 0)
        {
            contGame = false;
        }
        else if (num == 1)
        {
            setUpGame();
            gameCycle();
        }
    }

    public void setUpGame()
    {
        piezasSopa = new SingleLinkedList<>();
        piezasJuego = new SingleLinkedList<>();

        setUpPiezas();

        // Give Tiles to players
        shuffle();
        System.out.println(players.peekHead());
        players.resetIterator();
        for (int i = 0; i < players.size(); i++)
        {
            Player current = players.iterate();
            current.throwAwayPieces();
            for (int j = 0; j < 7; j++)
            {
                current.addPieza(piezasSopa.getHead());
            }
        }
    }

    public void startGame()
    {
        System.out.println("How many players?");
        int numPlayers = scanner.nextInt();
        // int numPlayers = 4;
        if (numPlayers > 4 || numPlayers < 2)
        {
            throw new IllegalArgumentException();
        }
        // Clean Buffer
        scanner.nextLine();
        // Create Players
        for (int i = 0; i < numPlayers; i++)
        {
            System.out.println("Player Name:");
            String name = scanner.nextLine();
            // String name = "";
            players.insertAtTail(new Player(name));
        }
        setUpGame();

        // Choose player with highest mula
        choooseHighestPlayer();
        System.out.println("Highest player done");
        gameCycle();
    }

    public void playerTurn(Player currentPlayer)
    {
        System.out.println("It's " + currentPlayer.getName() +
                           "'s turn");
        if (hasGoodPieces(currentPlayer))
        {
            System.out.println("User can place tiles");

        }
        else
        {
            System.out.println("(User cannot place tiles)");

        }
        Choices choice = currentPlayer.makeChoice();
        switch (choice)
        {
            case Play :
                playerTurnPutPieza(currentPlayer);
                break;
            case Skip :
                surrendered++;
                System.out.println(surrendered);
                break;
            case TakePiece :
                if (!piezasSopa.isEmpty())
                {
                    Pieza pieza = piezasSopa.getHead();
                    currentPlayer.addPieza(pieza);
                    System.out.println("You took a: " + pieza);
                }
                break;
        }
    }

    public boolean hasGoodPieces(Player currentPlayer)
    {
        boolean ans = false;
        currentPlayer.myPieces.resetIterator();
        Pieza begin = currentPlayer.myPieces.peekCurrent();
        Pieza current;

        while ((current = currentPlayer.myPieces.iterate()) != null)
        {
            if (isValidInsertion(Directions.LEFT, current) ||
                    isValidInsertion(Directions.RIGHT, current))
            {
                return true;
            }
        }
        return ans;
    }

    public void playerTurnPutPieza(Player currentPlayer)
    {
        // System.out.println("The game map is:");
        // System.out.println(piezasJuego);
        // System.out.println("Your pieces are");
        // System.out.println(currentPlayer.myPieces);
        Pieza tile = currentPlayer.choosePieza();
        Directions direction;
        if (piezasJuego.isEmpty())
        {
            direction = Directions.LEFT;
        }
        else
        {
            System.out.println("text");
            direction = currentPlayer.chooseDirection();
        }
        if (isValidInsertion(direction, tile))
        {
            // System.out.println("Valid Direction");
            putPieza(direction, tile);
            // System.out.println("Tile inserted, the map now is");
            // System.out.println(piezasJuego);
        }
        else
        {
            currentPlayer.addPieza(tile);
            System.out.println("Do you want to insert another piece? 0/1");
            int ans = scanner.nextInt();
            if (ans == 1)
            {
                playerTurnPutPieza(currentPlayer);
            }
            else
            {
                playerTurn(currentPlayer);
            }
        }
    }

    public void shuffle()
    {
        piezasSopa.shuffle();
    }

    @Override
    public String toString()
    {
        StringBuilder tmp = new StringBuilder();
        tmp.append(piezasJuego);
        return tmp.toString();
    }

    // NEW HEAD
    // (A-B)---(A-B)
    // TAIL NEW
    // (A-B)---(A-B)
    public boolean isValidInsertion(Directions dir, Pieza pieza)
    {
        if (piezasJuego.isEmpty())
            return true;
        if (dir == Directions.LEFT)
        {
            if (piezasJuego.peekHead().getNumeroA() == pieza.getNumeroB())
            {
                return true;
            }
            else
            {
                pieza.voltear();
                if (piezasJuego.peekHead().getNumeroA() == pieza.getNumeroB())
                    return true;
                else
                {
                    pieza.voltear();
                    return false;
                }
            }
        }
        else
        {
            if (piezasJuego.peekTail().getNumeroB() == pieza.getNumeroA())
            {
                return true;
            }
            else
            {
                pieza.voltear();
                if (piezasJuego.peekTail().getNumeroB() == pieza.getNumeroA())
                    return true;
                else
                {
                    pieza.voltear();
                    return false;
                }
            }
        }
    }

    public void putPieza(Pieza pieza)
    {
        piezasJuego.insertAtHead(pieza);
    }

    public void putPieza(Directions dir, Pieza pieza)
    {
        if (dir == Directions.LEFT)
        {
            piezasJuego.insertAtHead(pieza);
        }
        else
        {
            piezasJuego.insertAtTail(pieza);
        }
    }
}
