import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.rules.ExpectedException;
import org.junit.Rule;
import static org.junit.Assert.*;
import java.util.Random;
import java.util.LinkedList;

public class DominoGameTest
{
    private DominoGame myGame;
    @Before
    public void setupGame()
    {
        myGame = new DominoGame();
    }


    @Test
    public void fitTest()
    {
        myGame.piezasJuego.insertAtTail(new Pieza(Pieza.Numeros.UNO, Pieza.Numeros.DOS));
        System.out.println(myGame);
        Pieza derecha = new Pieza(Pieza.Numeros.DOS, Pieza.Numeros.CERO);
        Pieza izquierda = new Pieza(Pieza.Numeros.DOS, Pieza.Numeros.UNO);
        assertTrue(myGame.isValidInsertion(DominoGame.Directions.RIGHT,derecha ));
        assertTrue(myGame.isValidInsertion(DominoGame.Directions.LEFT,izquierda ));
    }

    @Test
    public void test_Case() {
    }

}
