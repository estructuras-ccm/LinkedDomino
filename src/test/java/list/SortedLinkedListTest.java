/*
 * SortedLinkedListTest.java
 * Copyright (C) 2017 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 */
package list;
import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import static org.junit.Assert.*;
import java.util.Random;
import org.junit.rules.ExpectedException;
import org.junit.Rule;

public class SortedLinkedListTest
{
    SortedList<Integer> myList;
    @Before
    public void createArray(){
        myList = new SortedLinkedList<Integer>();
        for (int i = 0; i < 10; i++) {
           myList.insert(i);
        }
    }

    @Test
    public void emptyTest()
    {
        //System.out.println(myList);
        Random rand = new Random();
        myList = new SortedLinkedList<Integer>();

        for (int i = 0; i < 4; i++) {
            myList.insert(i);
        }
        //System.out.println(myList);

        myList = new SortedLinkedList<Integer>();
        for (int i = 4; i > 0; i--) {
            myList.insert(i);
        }
        //System.out.println(myList);

        myList = new SortedLinkedList<Integer>();
        for (int i = 0; i < 10; i++) {
           myList.insert(rand.nextInt(100));
        }
        //System.out.println(myList);
    }

    @Test
    public void peekAtTest()
    {
        Random rand = new Random();
        int num = rand.nextInt(myList.size());
        System.out.println(myList);
        //System.out.println(num);
        //System.out.println(myList.size());
        assertTrue(num == myList.peekAt(num));
    }
}

