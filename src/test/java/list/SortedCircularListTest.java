package list;
import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import static org.junit.Assert.*;
import java.util.Random;
import org.junit.rules.ExpectedException;
import org.junit.Rule;

public class SortedCircularListTest
{
    SortedCircularList<Integer> myList;

    @Before
    public void createList()
    {
        myList = new SortedCircularList<>();
        for (int i = 0; i < 2; i++) {
            //myList.insert(i);
            //System.out.println(i);
        }
    }

    @Test
    public void emptyTest()
    {
        //System.out.println(myList);
        Random rand = new Random();
        myList = new SortedCircularList<Integer>();

        for (int i = 0; i < 4; i++) {
            myList.insert(i);
        }
        System.out.println(myList);

        myList = new SortedCircularList<Integer>();
        for (int i = 4; i > 0; i--) {
            myList.insert(i);
        }
        System.out.println(myList);

        myList = new SortedCircularList<Integer>();
        for (int i = 0; i < 4; i++) {
           myList.insert(rand.nextInt(100));
        }
        System.out.println(myList);
    }

    @Test
    public void iterateTest()
    {
        myList = new SortedCircularList<Integer>();
        int[] arr = new int[100];
        Random rand = new Random();
        for (int i = 0; i < 100; i++)
        {
            int num = rand.nextInt();
            arr[i] = num;
            myList.insert(num);
        }
        System.out.println(myList.size());
        for (int i = 0; i < 200; i++) {
            /*
             *System.out.println(myList.iterate());
             */
        }
    }
}
