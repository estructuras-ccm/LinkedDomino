/*
 * CircularListTest.java
 * Copyright (C) 2017 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 */

package list;
import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import static org.junit.Assert.*;
import java.util.Random;
import org.junit.rules.ExpectedException;
import org.junit.Rule;
import list.CircularList;
public class CircularListTest
{
    List<Integer> myList;
    @Rule
    public ExpectedException thrown = ExpectedException.none();


    @Before
    public void createList(){
        myList = new CircularList<>();
        for (int i = 0; i < 10; i++) {
            myList.insertAtTail(i);
        }
    }

    @Test
    public void peekAtHeadTest()
    {
        assertTrue(myList.peekHead() == 0);
    }


    @Test
    public void peekAtTailTest()
    {
        assertTrue(myList.peekTail() == 9);
    }

    @Test
    public void peekAtTest()
    {
        Random rand = new Random();
        int num = rand.nextInt(myList.size());
        assertTrue(myList.peekAt(num) == num);
    }

    @Test
    public void peekAtEmptyTest()
    {
        List<Integer> anotherList = new CircularList<>();
        assertNull(anotherList.peekHead());
        assertNull(anotherList.peekTail());
        Random rand = new Random();
        int position = rand.nextInt(100)+100;
        assertNull(anotherList.peekAt(position));
        thrown.expect(IndexOutOfBoundsException.class);
        myList.peekAt(position);
    }


    @Test
    public void insertAtHeadTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        myList.insertAtHead(num);
        assertTrue(myList.peekHead() == num);

        List<Integer> anotherList = new  CircularList<>();
        anotherList.insertAtHead(num);
        assertTrue(num == anotherList.peekHead());
    }

    @Test
    public void insertAtTailTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        myList.insertAtTail(num);
        assertTrue(myList.peekTail() == num);

        List<Integer> anotherList = new  CircularList<>();
        anotherList.insertAtTail(num);
        assertTrue(num == anotherList.peekTail());
    }

    @Test
    public void insertAtTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int position = rand.nextInt(myList.size());
        myList.insertAt(num, position);
        assertTrue(myList.peekAt(position) == num);
    }

    @Test
    public void insertAtEdgeHeadTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = 0;
        myList.insertAt(num, pos);
        int real = myList.peekAt(pos);
        assertTrue(real==num);

        myList = new CircularList<>();
        num = rand.nextInt();
        pos = 0;
        myList.insertAt(num, pos);
        real = myList.peekAt(pos);
        assertTrue(real==num);

    }

    @Test
    public void insertAtEdgeTailTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = myList.size();
        myList.insertAt(num, pos);
        int real = myList.peekAt(pos);
        assertTrue(real==num);

        myList = new CircularList<>();
        num = rand.nextInt();
        pos = myList.size();
        myList.insertAt(num, pos);
        real = myList.peekAt(pos);
        assertTrue(real==num);
    }

   @Test
    public void insetAtEdgeAtTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        myList.insertAt(num, myList.size()-1);
        assertTrue(myList.peekAt(myList.size()-2) == num);
        int pos = myList.size()+1;
        thrown.expect(IndexOutOfBoundsException.class);
        myList.insertAt(num, pos);
        /* int real = myList.peekAt(pos); */
        /* assertTrue(real==num); */
    }

    //Edge case when no head
    //Edge case when only head
    @Test
    public void setHeadTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        myList.setHead(num);
        assertTrue(myList.peekHead() == num);

        List<Integer> anotherList = new CircularList<>();
        anotherList.setHead(num);
        assertTrue(anotherList.peekHead() == num);
        anotherList.setHead(num+1);
        assertTrue(anotherList.peekHead() == num+1);

    }

    @Test
    public void setTail()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        myList.setTail(num);
        assertTrue(myList.peekTail() == num);

        List<Integer> anotherList = new CircularList<>();
        anotherList.setTail(num);
        assertTrue(anotherList.peekTail() == num);
    }


    //Edge case when position is
    //  head
    //  tail
    //  bigger than tail
    //OR
    //list is empty
    //  same conditions
    @Test
    public void setAtTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = rand.nextInt(myList.size());
        myList.setAt(num, pos);
        assertTrue(myList.peekAt(pos) == num);
        pos = 0;
        myList.setAt(num, pos);
        assertTrue(myList.peekAt(pos) == num);
        pos = myList.size()-1;
        myList.setAt(num, pos);
        assertTrue(myList.peekAt(pos) == num);

        List<Integer> anotherList = new CircularList<>();
        pos = 0;
        anotherList.setAt(num, pos);
        assertTrue(anotherList.peekAt(pos) == num);

        anotherList = new CircularList<>();
        pos = anotherList.size();
        anotherList.setAt(num, pos);
        assertTrue(anotherList.peekAt(pos) == num);

        anotherList.insertAtTail(2);
        pos = anotherList.size();
        thrown.expect(IndexOutOfBoundsException.class);
        anotherList.setAt(num, pos);
    }

    @Test
    public void getHeadTest()
    {
        int oldHead = myList.getHead();
        assertFalse(oldHead == myList.peekHead());
    }

    @Test
    public void getTailTest()
    {
        int oldTail = myList.getTail();
        assertFalse(oldTail == myList.peekTail());
    }

    @Ignore
    @Test
    public void getAtTest()
    {
        Random rand = new Random();
        int position = rand.nextInt(myList.size());
        int oldAt = myList.getAt(position);
        assertFalse(oldAt == myList.peekAt(position));

        position = myList.size()-1;
        oldAt = myList.getAt(position);
        assertTrue(myList.peekAt(position-1) != oldAt);

    }

    // Edge cases:
    // When size = 0;
    // When size = 1;
    @Test
    public void getAtHeadEdgeTest()
    {
        List<Integer> anotherList = new CircularList<>();
        assertNull(anotherList.getHead());
        anotherList.insertAtHead(1);
        assertTrue(anotherList.getHead() == 1);
    }

    // Edge cases:
    // When size = 0;
    // When size = 1;
      @Test
    public void getAtTailEdgeTest()
    {
        List<Integer> anotherList = new CircularList<>();
        assertNull(anotherList.getTail());
        anotherList.insertAtTail(1);
        /* System.out.println(anotherList); */
        assertTrue(anotherList.getTail() == 1);
    }

    // Edge cases:
    // When size = 0;
    // When size = 1;
    // When position = 0;
    // When position = size;
    // When position = size-1;
    // When position > size;
    @Test
    public void getAtEdgeTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int position = 0;
        List<Integer> anotherList = new CircularList<>();
        assertNull(anotherList.getAt(position));
        anotherList.insertAtHead(1);
        anotherList.getAt(0);
        anotherList.getAt(0);
        anotherList.insertAtTail(1);
        anotherList.insertAtTail(2);

        thrown.expect(IndexOutOfBoundsException.class);
        anotherList.insertAtTail(1);
        anotherList.getAt(10);
    }





    @Test
    public void findKeyPositionTest()
    {
        Random rand = new Random();
        int position = rand.nextInt(myList.size());
        int ans = myList.findKeyPosition(position);
        assertTrue(position == ans);

        position = 0;
        ans = myList.findKeyPosition(position);
        assertTrue(position == ans);

        position = myList.size()-1;
        ans = myList.findKeyPosition(position);
        assertTrue(position== ans);
    }


}
