package list;
import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.rules.ExpectedException;
import org.junit.Rule;
import static org.junit.Assert.*;
import java.util.Random;
import java.util.LinkedList;
import list.DoubleLinkedList;
import list.DoubleLinkedList.DoubleLinkedListIterator;

public class DoubleLinkedListTest {
    DoubleLinkedList<Integer> myList;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void createList()
    {
        myList = new DoubleLinkedList<>();
        for (int i = 0; i < 10; i++) {
            myList.insertAtTail(i);
        }
    }


    @Test
    public void peekAtHeadTest()
    {
        assertTrue(myList.peekHead() == 0);
    }

    @Test
    public void peekAtTail()
    {
        assertTrue(myList.peekTail() == 9);

    }

     @Test
     public void peekAtTest()
     {
         Random rand = new Random();
         int num = rand.nextInt(myList.size());
         // System.out.println(myList.size());

         assertTrue(myList.peekAt(num) == num);
     }

    @Test
    public void insertAtTailTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        myList.insertAtTail(num);
        assertTrue(num == myList.peekTail());

        List<Integer> anotherList = new DoubleLinkedList<>();
        anotherList.insertAtTail(num);
        assertTrue(num == anotherList.peekTail());
    }

    @Test
    public void insertAtHeadTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        myList.insertAtHead(num);
        assertTrue(num == myList.peekHead());

        DoubleLinkedList<Integer> anotherList = new DoubleLinkedList<>();
        anotherList.insertAtHead(num);
        assertTrue(num == anotherList.peekHead());

    }

    @Test
    public void insertAtTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = rand.nextInt(myList.size());
        // System.out.println(myList);
        // System.out.println(num);
        // System.out.println(pos);
        myList.insertAt(num, pos);
        int real = myList.peekAt(pos);
        // System.out.println(myList);

        assertTrue(real == num);
    }


    @Test
    public void insertAtEdgeHeadTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = 0;
        myList.insertAt(num, pos);
        int real = myList.peekAt(pos);
        assertTrue(real == num);

        myList = new DoubleLinkedList<>();
        num = rand.nextInt();
        pos = 0;
        myList.insertAt(num, pos);
        real = myList.peekAt(pos);
        assertTrue(real == num);

    }

    @Test
    public void insertAtEdgeTailTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = myList.size();
        myList.insertAt(num, pos);
        int real = myList.peekAt(pos);
        assertTrue(real == num);

        myList = new DoubleLinkedList<>();
        num = rand.nextInt();
        pos = myList.size();
        myList.insertAt(num, pos);
        real = myList.peekAt(pos);
        assertTrue(real == num);
    }

    @Test
    public void insertAtEdgeAtTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = myList.size() + 1;
        thrown.expect(IndexOutOfBoundsException.class);
        myList.insertAt(num, pos);
        /* int real = myMyList.peekAt(pos); */
        /* assertTrue(real==num); */
    }

    @Test
    public void getHeadTest()
    {
        int oldHead = myList.getHead();
        assertTrue(myList.getHead() != oldHead);
    }

    @Test
    public void getTailTest()
    {
        int oldTail = myList.getTail();
        assertTrue(myList.getTail() != oldTail);
    }

    @Ignore
    @Test
    public void getAtTest()
    {
        Random rand = new Random();
        int pos = rand.nextInt(myList.size() - 1);
        int oldNum = myList.getAt(pos);
        assertTrue(myList.peekAt(pos) != oldNum);

        pos = myList.size() - 1;
        oldNum = myList.getAt(pos);
        assertTrue(myList.peekAt(pos - 1) != oldNum);
    }

    // Edge cases:
    // When size = 0;
    // When size = 1;
    @Test
    public void getAtHeadEdgeTest()
    {
        DoubleLinkedList<Integer> anotherList =
            new DoubleLinkedList<>();
        // assertNull(anotherList.getHead());
        anotherList.insertAtHead(1);
        assertTrue(anotherList.getHead() == 1);
    }

    // Edge cases:
    // When size = 0;
    // When size = 1;
    @Test
    public void getAtTailEdgeTest()
    {
        DoubleLinkedList<Integer> anotherList =
            new DoubleLinkedList<>();
        // assertNull(anotherList.getTail());
        anotherList.insertAtTail(1);
        System.out.println(anotherList.peekTail());
        System.out.println(anotherList.peekTail());

        // assertTrue(anotherList.getTail() == 1);
    }

    // Edge cases:
    // When size = 0;
    // When size = 1;
    // When position = 0;
    // When position = size;
    // When position = size-1;
    // When position > size;
    @Test
    public void getAtEdgeTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int position = 0;
        DoubleLinkedList<Integer> anotherList =
            new DoubleLinkedList<>();
        // assertNull(anotherList.getAt(position));
        anotherList.insertAtHead(1);
        anotherList.getAt(0);
        /* anotherMyList.getAt(10); */
        anotherList.insertAtTail(1);
        anotherList.insertAtTail(2);

        thrown.expect(IndexOutOfBoundsException.class);
        anotherList.insertAtTail(1);
        anotherList.getAt(10);
    }
    // Edge case when no head
    // Edge case when only head
    @Test
    public void setHeadTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        myList.setHead(num);
        assertTrue(myList.peekHead() == num);
        DoubleLinkedList<Integer> anotherList = new DoubleLinkedList<>();
        System.out.println(anotherList);
        // anotherList.setHead(num);
        System.out.println(anotherList);
        // assertTrue(anotherList.peekHead() == num);
        // anotherList.setHead(num + 1);
        // assertTrue(anotherList.peekHead() == num + 1);
    }

    @Test
    public void setTailTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        myList.setTail(num);
        assertTrue(myList.peekTail() == num);

        DoubleLinkedList<Integer> anotherList = new DoubleLinkedList<>();
        // anotherList.setTail(num);
        // assertTrue(anotherList.peekTail() == num);

    }

    // Edge case when position is
    // head
    // tail
    // bigger than tail
    // OR
    // myList is empty
    // same conditions
    @Test
    public void setAtTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = rand.nextInt(myList.size());
        myList.setAt(num, pos);
        assertTrue(myList.peekAt(pos) == num);
        pos = 0;
        myList.setAt(num, pos);
        assertTrue(myList.peekAt(pos) == num);
        pos = myList.size() - 1;
        myList.setAt(num, pos);
        assertTrue(myList.peekAt(pos) == num);

        DoubleLinkedList<Integer> anotherList = new DoubleLinkedList<>();
        // pos = 0;
        // anotherList.setAt(num, pos);
        // assertTrue(anotherList.peekAt(pos) == num);

        // anotherList = new DoubleLinkedList<>();
        // pos = anotherList.size();
        // anotherList.setAt(num, pos);
        // assertTrue(anotherList.peekAt(pos) == num);

        // System.out.println(anotherList);
        // anotherList.insertAtTail(2);
        // pos = anotherList.size();
        // thrown.expect(IndexOutOfBoundsException.class);
        // anotherList.setAt(num, pos);
    }

    // Edge case when last element
    @Test
    public void findKeyPositionTest()
    {
        Random rand = new Random();
        int pos = rand.nextInt(myList.size());
        int place = myList.findKeyPosition(pos);

        // System.out.println(pos);

        assertTrue(pos == place);
    }
}
