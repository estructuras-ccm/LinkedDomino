/*
 * SingleLinkedListTest.java
 * Copyright (C) 2017 sergio <sergio@evoMacSergio>
 *
 * Distributed under terms of the MIT license.
 * TODO: Test and define edge cases (example empty list, one element, two elements)
 */
package list;
import org.junit.Before;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.rules.ExpectedException;
import org.junit.Rule;
import static org.junit.Assert.*;
import java.util.Random;
import java.util.LinkedList;

public class SingleLinkedListTest
{
    private SingleLinkedList<Integer> list;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void createNumericSortedList() {
        list = new SingleLinkedList<Integer>();
        for (int i = 0; i < 10; i++) {
            list.insertAtTail(i);
        }
    }

    @After
    public void printResults()
    {
        /* System.out.println("Size: " +list.size()); */
        /* System.out.println(list.toString()); */
    }

    @Test
    public void nodeTest()
    {
        Node<Integer> tmp = new Node<Integer>(1);
        assertTrue(tmp.getData().equals(1));
        assertTrue(tmp.getData() == 1);
    }

    @Test
    public void peekAtHeadTest()
    {
        assertTrue(list.peekHead() == 0);
    }

    @Test
    public void peekAtTail()
    {
        assertTrue(list.peekTail() == 9);

    }

    @Test
    public void peekAtTest()
    {
        Random rand = new Random();
        int num = rand.nextInt(list.size());
        assertTrue(list.peekAt(num) == num);
    }

    @Test
    public void peekAtEmptyTest()
    {
        List<Integer> anotherList = new SingleLinkedList<>();
        assertNull(anotherList.peekHead());
        assertNull(anotherList.peekTail());
        Random rand = new Random();
        int position = rand.nextInt(100) + 100;
        assertNull(anotherList.peekAt(position));
        thrown.expect(IndexOutOfBoundsException.class);
        list.peekAt(position);
    }

    @Test
    public void insertAtTailTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        list.insertAtTail(num);
        assertTrue(num == list.peekTail());

        List<Integer> anotherList = new SingleLinkedList<>();
        anotherList.insertAtTail(num);
        assertTrue(num == anotherList.peekTail());
    }

    @Test
    public void insertAtHeadTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        list.insertAtHead(num);
        assertTrue(num == list.peekHead());

        List<Integer> anotherList = new SingleLinkedList<>();
        anotherList.insertAtHead(num);
        assertTrue(num == anotherList.peekHead());

    }

    @Test
    public void insertAtTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = rand.nextInt(list.size());
        list.insertAt(num, pos);
        int real = list.peekAt(pos);
        assertTrue(real == num);
    }

    @Test
    public void insertAtEdgeHeadTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = 0;
        list.insertAt(num, pos);
        int real = list.peekAt(pos);
        assertTrue(real == num);

        list = new SingleLinkedList<>();
        num = rand.nextInt();
        pos = 0;
        list.insertAt(num, pos);
        real = list.peekAt(pos);
        assertTrue(real == num);

    }

    @Test
    public void insertAtEdgeTailTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = list.size();
        list.insertAt(num, pos);
        int real = list.peekAt(pos);
        assertTrue(real == num);

        list = new SingleLinkedList<>();
        num = rand.nextInt();
        pos = list.size();
        list.insertAt(num, pos);
        real = list.peekAt(pos);
        assertTrue(real == num);
    }

    @Test
    public void insetAtEdgeAtTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = list.size() + 1;
        thrown.expect(IndexOutOfBoundsException.class);
        list.insertAt(num, pos);
        /* int real = list.peekAt(pos); */
        /* assertTrue(real==num); */
    }

    @Test
    public void getHeadTest()
    {
        int oldHead = list.getHead();
        assertTrue(list.getHead() != oldHead);
    }

    @Test
    public void getTailTest()
    {
        int oldTail = list.getTail();
        assertTrue(list.getTail() != oldTail);
    }

    @Test
    public void getAtTest()
    {
        Random rand = new Random();
        int pos = rand.nextInt(list.size() - 1);
        int oldNum = list.getAt(pos);
        assertTrue(list.peekAt(pos) != oldNum);

        pos = list.size() - 1;
        oldNum = list.getAt(pos);
        assertTrue(list.peekAt(pos - 1) != oldNum);
    }

    // Edge cases:
    // When size = 0;
    // When size = 1;
    @Test
    public void getAtHeadEdgeTest()
    {
        List<Integer> anotherList = new SingleLinkedList<>();
        assertNull(anotherList.getHead());
        anotherList.insertAtHead(1);
        assertTrue(anotherList.getHead() == 1);
    }

    // Edge cases:
    // When size = 0;
    // When size = 1;
    @Test
    public void getAtTailEdgeTest()
    {
        List<Integer> anotherList = new SingleLinkedList<>();
        assertNull(anotherList.getTail());
        anotherList.insertAtTail(1);
        /* System.out.println(anotherList); */
        assertTrue(anotherList.getTail() == 1);
    }

    // Edge cases:
    // When size = 0;
    // When size = 1;
    // When position = 0;
    // When position = size;
    // When position = size-1;
    // When position > size;
    @Test
    public void getAtEdgeTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int position = 0;
        List<Integer> anotherList = new SingleLinkedList<>();
        assertNull(anotherList.getAt(position));
        anotherList.insertAtHead(1);
        anotherList.getAt(0);
        anotherList.getAt(0);
        /* anotherList.getAt(10); */
        anotherList.insertAtTail(1);
        anotherList.insertAtTail(2);

        thrown.expect(IndexOutOfBoundsException.class);
        anotherList.insertAtTail(1);
        anotherList.getAt(10);
    }

    // Edge case when no head
    // Edge case when only head
    @Test
    public void setHeadTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        list.setHead(num);
        assertTrue(list.peekHead() == num);
        List<Integer> anotherList = new SingleLinkedList<>();
        anotherList.setHead(num);
        assertTrue(anotherList.peekHead() == num);
        anotherList.setHead(num + 1);
        assertTrue(anotherList.peekHead() == num + 1);
    }

    @Test
    public void setTailTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        list.setTail(num);
        assertTrue(list.peekTail() == num);

        List<Integer> anotherList = new SingleLinkedList<>();
        anotherList.setTail(num);
        assertTrue(anotherList.peekTail() == num);

    }

    // Edge case when position is
    // head
    // tail
    // bigger than tail
    // OR
    // list is empty
    // same conditions
    @Test
    public void setAtTest()
    {
        Random rand = new Random();
        int num = rand.nextInt();
        int pos = rand.nextInt(list.size());
        list.setAt(num, pos);
        assertTrue(list.peekAt(pos) == num);
        pos = 0;
        list.setAt(num, pos);
        assertTrue(list.peekAt(pos) == num);
        pos = list.size() - 1;
        list.setAt(num, pos);
        assertTrue(list.peekAt(pos) == num);

        List<Integer> anotherList = new SingleLinkedList<>();
        pos = 0;
        anotherList.setAt(num, pos);
        assertTrue(anotherList.peekAt(pos) == num);

        anotherList = new SingleLinkedList<>();
        pos = anotherList.size();
        anotherList.setAt(num, pos);
        assertTrue(anotherList.peekAt(pos) == num);

        System.out.println(anotherList);
        anotherList.insertAtTail(2);
        pos = anotherList.size();
        thrown.expect(IndexOutOfBoundsException.class);
        anotherList.setAt(num, pos);
    }

    @Test
    public void shuffleTest()
    {
        list.shuffle();
    }

    // Edge case when last element
    @Test
    public void findKeyPositionTest()
    {
        Random rand = new Random();
        int pos = 9;
        int place = list.findKeyPosition(pos);
        assertTrue(pos == place);
    }
}
